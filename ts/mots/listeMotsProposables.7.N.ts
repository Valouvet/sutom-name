export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"N'DIAYE",
"N'DRICK",
"N'NAMOU",
"N'NESTA",
"NABELLA",
"NABILAH",
"NABILLA",
"NABILLE",
"NABITOU",
"NACEIRA",
"NACHIDA",
"NACIERA",
"NADEIGE",
"NADEJDA",
"NADETTE",
"NADHIRA",
"NADIANA",
"NADIEGE",
"NADJATE",
"NADJATI",
"NADJETE",
"NADJIBA",
"NADJIDA",
"NADJILA",
"NADJIMA",
"NADJIME",
"NADJOUA",
"NAELINE",
"NAELLIA",
"NAELLYA",
"NAELLYS",
"NAELYAH",
"NAELYNE",
"NAFISSA",
"NAFTALI",
"NAFYSSA",
"NAGETTE",
"NAGIHAN",
"NAHELLE",
"NAHOMIE",
"NAHOUEL",
"NAJATTE",
"NAJETTE",
"NALIYAH",
"NAMISSA",
"NANDINI",
"NANETTE",
"NANTENE",
"NAOELLE",
"NAOILLE",
"NAOLINE",
"NAOLYNE",
"NAOPHEL",
"NAOUALE",
"NAOUELE",
"NAOUFAL",
"NAOUFEL",
"NAOUMIE",
"NAOUMOU",
"NAOURES",
"NARAYAN",
"NARCISA",
"NARCISO",
"NARDJES",
"NARGESS",
"NARIMAN",
"NARIMEL",
"NARIMEN",
"NARJESS",
"NARJISS",
"NARMINE",
"NASCIMO",
"NASLATI",
"NASRATI",
"NASREEN",
"NASRINA",
"NASRINE",
"NASSERA",
"NASSERE",
"NASSIBA",
"NASSIHA",
"NASSIKA",
"NASSILA",
"NASSIMA",
"NASSIME",
"NASSIRA",
"NATACHA",
"NATALIA",
"NATALIE",
"NATALIO",
"NATALYA",
"NATANEL",
"NATASHA",
"NATHAEL",
"NATHALY",
"NATHANE",
"NATHANN",
"NATIDJA",
"NATSUKI",
"NATSUMI",
"NATTHAN",
"NAUREEN",
"NAURINE",
"NAUSICA",
"NAWALLE",
"NAWELLE",
"NAWRESS",
"NAYANKA",
"NAYANNE",
"NAYELIE",
"NAYELLE",
"NAYLANN",
"NAYLINE",
"NAZAIRE",
"NAZARIO",
"NAZMIYE",
"NEBAHAT",
"NEBOJSA",
"NEDJEMA",
"NEDJIMA",
"NEDJOUA",
"NEFISSA",
"NEGRITA",
"NEHEMIA",
"NEHEMIE",
"NELIYAH",
"NELLYAH",
"NEMANJA",
"NEMESIS",
"NEMORIN",
"NENETTE",
"NEPHTYS",
"NERIMAN",
"NERIMEN",
"NERLINE",
"NERMINA",
"NERMINE",
"NESAYEM",
"NESRINE",
"NESRYNE",
"NESSIMA",
"NESSIME",
"NESSRIN",
"NESTABA",
"NETANEL",
"NEVENKA",
"NEVENOE",
"NEVILLE",
"NEYLANN",
"NEYLIAH",
"NEZAHAT",
"NEZAKET",
"NIAGALE",
"NIAGARA",
"NIAKALE",
"NIARALE",
"NICAISE",
"NICANOR",
"NICCOLO",
"NICETTE",
"NICKSON",
"NICODEM",
"NICOLAE",
"NICOLAI",
"NICOLAS",
"NICOLAY",
"NICOLLE",
"NIELSEN",
"NIHELLE",
"NIKODEM",
"NIKOLAI",
"NIKOLAS",
"NIKOLAY",
"NIKOLAZ",
"NIKOLOZ",
"NILUFER",
"NINELLE",
"NINETTA",
"NINETTE",
"NIRMALA",
"NIRMINE",
"NIROJAN",
"NIRUJAN",
"NIRVANA",
"NISANUR",
"NISETTE",
"NISRINA",
"NISRINE",
"NISSRIN",
"NITHISH",
"NIVETHA",
"NOALINE",
"NOAMANE",
"NOELANE",
"NOELANN",
"NOELINE",
"NOELISE",
"NOELLIA",
"NOELLIE",
"NOELLYA",
"NOELYNE",
"NOELYNN",
"NOELYSE",
"NOHAILA",
"NOHAYLA",
"NOHEILA",
"NOHELLA",
"NOHEMIE",
"NOHEYLA",
"NOHLANN",
"NOLANNE",
"NOLHANE",
"NOLHANN",
"NOLLANN",
"NOLVENN",
"NOLWEEN",
"NOLWENE",
"NOLWENN",
"NOMINOE",
"NOODLES",
"NORANNE",
"NORBERT",
"NORCINE",
"NORDAHL",
"NORDINE",
"NORHANE",
"NORHENE",
"NORIANE",
"NORLANE",
"NORMAND",
"NORMANE",
"NORMANN",
"NORSINE",
"NORYANE",
"NOUAMAN",
"NOUCHKA",
"NOUHOUM",
"NOUJOUD",
"NOUMANE",
"NOURANE",
"NOURAYA",
"NOURDIN",
"NOURHAN",
"NOURHEN",
"NOURINE",
"NOURIYA",
"NOVALIE",
"NUMIDIA",
"NURSENA",
"NURSIMA",
"NUSAYBA",
"NYCOLAS",
"NYMPHEA",
"NYSRINE",
];
}