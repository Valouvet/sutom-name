export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"TABATHA",
"TABITHA",
"TACIANA",
"TADENSZ",
"TADEUSZ",
"TAHIANA",
"TAHICIA",
"TAHIROU",
"TAHISSA",
"TAHISSE",
"TAHITIA",
"TAHITOA",
"TAIRICK",
"TAISSIA",
"TAISSIR",
"TAISSYA",
"TAKESHI",
"TALIANA",
"TALIANE",
"TALICIA",
"TALISSA",
"TALITHA",
"TALIYAH",
"TALYANA",
"TALYSSA",
"TAMARAH",
"TAMATEA",
"TAMATOA",
"TAMILLA",
"TAMISHA",
"TANINNA",
"TANISHA",
"TANTELY",
"TANYSHA",
"TANZILA",
"TAOUFIC",
"TAOUFIK",
"TAOUFIQ",
"TAOUSSE",
"TASIANA",
"TASLIMA",
"TASLIME",
"TASMINE",
"TASNEEM",
"TASNIME",
"TASNYME",
"TASSILO",
"TASSNIM",
"TATIANA",
"TATIANE",
"TATJANA",
"TATYANA",
"TAYANNA",
"TAYLANN",
"TAYLEUR",
"TAYMIYA",
"TAYMOUR",
"TAYRICK",
"TAYRONE",
"TAYRONN",
"TAYSSIA",
"TAYSSIR",
"TCHELSY",
"TEEYANA",
"TENENAN",
"TENESSY",
"TEODORA",
"TEODORO",
"TEOFILO",
"TERANCE",
"TERENCE",
"TESLIME",
"TESNIME",
"TESNYME",
"TESSANE",
"TESSNIM",
"TESSNYM",
"TEYMOUR",
"TEYSSIR",
"THADDEE",
"THADEUS",
"THAILYS",
"THAIRON",
"THAIRYS",
"THAISSA",
"THAISSE",
"THAISSY",
"THALINA",
"THALITA",
"THALIYA",
"THALLIA",
"THALLYA",
"THALYNA",
"THAMARA",
"THAMEUR",
"THAMILA",
"THANAEL",
"THANAIS",
"THANINA",
"THANIYA",
"THARSAN",
"THAWBAN",
"THAYANA",
"THAYLOR",
"THAYRON",
"THAYSSA",
"THELIAU",
"THEMIRE",
"THENAIS",
"THEODEN",
"THEODOR",
"THEONIE",
"THEOTIM",
"THERESA",
"THERESE",
"THEREZA",
"THEREZE",
"THESSIA",
"THIBALD",
"THIBALT",
"THIBAUD",
"THIBAUT",
"THIBERT",
"THIEFEN",
"THIERNO",
"THIERRI",
"THIERRY",
"THIFANY",
"THILELI",
"THILYAN",
"THIMAEL",
"THIMOTE",
"THIMOTY",
"THINESH",
"THIVIYA",
"THIZIRI",
"THOBIAS",
"THOMASE",
"THOMASS",
"THOMINE",
"THORAYA",
"THORGAL",
"THOURIA",
"THURIAU",
"THYANNA",
"THYBALT",
"THYBAUD",
"THYBAUT",
"THYLANE",
"THYLANN",
"THYLIAN",
"THYMAEL",
"THYMOTE",
"TIAVINA",
"TIBAULT",
"TIBERIO",
"TIBURCE",
"TICIANA",
"TICIANO",
"TIDIANE",
"TIDIANI",
"TIDIANY",
"TIDJANE",
"TIDJANI",
"TIDJANY",
"TIDYANE",
"TIDYANN",
"TIEMOKO",
"TIFAINE",
"TIFANIE",
"TIFANNY",
"TIFENNE",
"TIFFANI",
"TIFFANY",
"TIFFENE",
"TIFFENN",
"TIGRANE",
"TIGUIDA",
"TIGUIDE",
"TIJANIA",
"TILELLI",
"TILIANA",
"TILIANN",
"TILIANO",
"TILLIAN",
"TILOUAN",
"TILYANN",
"TIMELIO",
"TIMOFEY",
"TIMOTEE",
"TIMOTEI",
"TIMOTEO",
"TIMOTEY",
"TIMOTHE",
"TIMOTHY",
"TIMUCIN",
"TIPHANY",
"TIPHENE",
"TIPHENN",
"TIRANKE",
"TITAINA",
"TITANIA",
"TITIANA",
"TITIANE",
"TITOINE",
"TITOUAN",
"TIVIZIO",
"TIZIANA",
"TIZIANO",
"TOBIASZ",
"TOIMAYA",
"TOLUNAY",
"TOMMASO",
"TORSTEN",
"TOSCANE",
"TOUATIA",
"TOUFFIK",
"TOUFICK",
"TOUFIKE",
"TOUFIRK",
"TOUHAMI",
"TOULINE",
"TOUMANI",
"TOUMANY",
"TOUNSIA",
"TOURAYA",
"TOURIYA",
"TOURKIA",
"TRAYVIS",
"TREMEUR",
"TRESSIA",
"TRESSIE",
"TRESSYA",
"TRESTAN",
"TREVEUR",
"TREYVIS",
"TRINIDA",
"TRINITE",
"TRINITY",
"TRISSIA",
"TRISTAN",
"TRYSSIA",
"TRYSTAN",
"TSIPORA",
"TSOLINE",
"TUGDUAL",
"TUNAHAN",
"TURENNE",
"TYDIANE",
"TYDJANE",
"TYFAINE",
"TYFANIE",
"TYFFANY",
"TYFFENE",
"TYFFENN",
"TYLIANA",
"TYLIANE",
"TYLIANN",
"TYLIANO",
"TYLLIAN",
"TYMOTHE",
"TYPHENE",
"TYPHENN",
"TYREESE",
"TYRONNE",
"TYTOUAN",
"TYZIANO",
];
}