export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"O'NEAL",
"O'NEIL",
"OANELL",
"OBEIDA",
"OBRYAN",
"OCCULI",
"OCEANA",
"OCEANE",
"OCEANN",
"OCEANO",
"OCEANY",
"OCELIA",
"OCELYA",
"OCILIA",
"OCTAVE",
"OCULIA",
"OCULIE",
"ODALYS",
"ODELIA",
"ODELIE",
"ODELIN",
"ODELYA",
"ODERIC",
"ODESSA",
"ODETTE",
"ODILIA",
"ODILLE",
"ODILON",
"ODRICK",
"ODYLLE",
"OFELIA",
"OFELIE",
"OHANNA",
"OHIANA",
"OIHANA",
"OIHANE",
"OIHIBA",
"OIHIDA",
"OIRDIA",
"OISILA",
"OISSIM",
"OKACHA",
"OKSANA",
"OLEANE",
"OLENKA",
"OLIANA",
"OLIANE",
"OLINDA",
"OLINDO",
"OLINKA",
"OLIVAN",
"OLIVER",
"OLIVIA",
"OLIVIO",
"OLIVYA",
"OLIWIA",
"OLWENN",
"OLYANA",
"OLYMPE",
"OLYNDA",
"OLYVIA",
"OMAIMA",
"OMAYMA",
"OMAYRA",
"OMEIMA",
"OMEYMA",
"OMNIYA",
"OMRANE",
"ONDINA",
"ONDINE",
"ONELIA",
"ONESSA",
"OONAGH",
"OPHELY",
"ORACIO",
"ORACLE",
"ORANNE",
"ORASIE",
"ORATIO",
"ORAZIO",
"OREADE",
"OREANE",
"OREGAN",
"ORELIA",
"ORELIE",
"ORENZO",
"ORESTE",
"ORIANA",
"ORIANE",
"ORIANO",
"ORIENT",
"ORLANA",
"ORLAND",
"ORLANE",
"ORLANN",
"ORLENA",
"ORLINE",
"ORNELA",
"OROKIA",
"ORPHEA",
"ORPHEE",
"ORPHEO",
"ORSOLA",
"ORYANA",
"ORYANE",
"OSANNA",
"OSANNE",
"OSIRIS",
"OSMANE",
"OSMANN",
"OSSAMA",
"OSSANA",
"OSSIAN",
"OSSMAN",
"OSVALD",
"OSWALD",
"OTALIA",
"OTAVIO",
"OTELLO",
"OTHMAN",
"OTHMEN",
"OTILIA",
"OTILIE",
"OTMANE",
"OTNIEL",
"OTTMAN",
"OTTMAR",
"OTYLIA",
"OUADAH",
"OUADIE",
"OUADIH",
"OUAFAA",
"OUAFAE",
"OUAFFA",
"OUAFIA",
"OUAHAB",
"OUAHBI",
"OUAHEB",
"OUAHIB",
"OUAHID",
"OUAIBA",
"OUAJDI",
"OUALID",
"OUAMAR",
"OUANES",
"OUARDA",
"OUARDI",
"OUARIA",
"OUBAYD",
"OUBEYD",
"OUELID",
"OUIAME",
"OUICEM",
"OUIDAD",
"OUIDED",
"OUISAM",
"OUISEM",
"OULAYA",
"OULEYE",
"OUMAMA",
"OUMARE",
"OUMAYA",
"OUMEYA",
"OUMMOU",
"OUMNIA",
"OUMNYA",
"OUMOUL",
"OUNAYS",
"OUNEYS",
"OURDIA",
"OURIDA",
"OURIEL",
"OUSAMA",
"OUSMAN",
"OUSSAM",
"OUSSEM",
"OUTMAN",
"OUWAIS",
"OUWAYS",
"OUWEIS",
"OUWEYS",
"OUZIEL",
"OVADIA",
"OVIDIO",
"OVIDIU",
"OWAISS",
"OWAYSS",
"OWEISS",
"OWEYSS",
"OXANNA",
"OXANNE",
"OXENCE",
"OYANNA",
"OYHANA",
"OZALEE",
"OZANNA",
"OZANNE",
];
}