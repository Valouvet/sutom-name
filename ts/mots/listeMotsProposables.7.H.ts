export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"HABIBOU",
"HACHEMI",
"HACHIME",
"HACHIMI",
"HACHMIA",
"HADAMOU",
"HADASSA",
"HADEMOU",
"HADHOUM",
"HADIDJA",
"HADJARA",
"HADJERA",
"HADJILA",
"HADJIRA",
"HADRIAN",
"HADRIEL",
"HADRIEN",
"HAFFIDA",
"HAFIDHA",
"HAFSOIT",
"HAICKEL",
"HAIETTE",
"HAILLEY",
"HAISSAM",
"HAITHAM",
"HAITHEM",
"HALEEMA",
"HALILOU",
"HALVARD",
"HAMADOU",
"HAMARIA",
"HAMDANE",
"HAMELLE",
"HAMIDOU",
"HAMMADI",
"HAMOUDA",
"HAMOUDI",
"HANAINE",
"HANALEI",
"HANANNE",
"HANISSA",
"HANITRA",
"HANNANE",
"HANNENE",
"HANNIEL",
"HANSLEY",
"HAOUARI",
"HARLEEN",
"HARMONY",
"HAROUNA",
"HAROUNE",
"HARRIET",
"HARROLD",
"HASDINE",
"HASHLEY",
"HASMINA",
"HASNAIN",
"HASSANA",
"HASSANE",
"HASSANI",
"HASSEIN",
"HASSENA",
"HASSENE",
"HASSIBA",
"HASSINA",
"HASSINE",
"HASSNAA",
"HATOUMA",
"HAVANNA",
"HAVANUR",
"HAYATTE",
"HAYDENN",
"HAYETTE",
"HAYLANA",
"HAYLEEN",
"HAYLINE",
"HAYRIYE",
"HAYSSAM",
"HAYTHAM",
"HAYTHEM",
"HAZDINE",
"HEATHER",
"HECHAME",
"HEDVIGE",
"HEDWIGE",
"HEIARII",
"HEILANI",
"HEIMANA",
"HEIMITI",
"HEITHEM",
"HELEANA",
"HELEINE",
"HELENNA",
"HELENNE",
"HELIADE",
"HELIANA",
"HELIANE",
"HELICIA",
"HELIENA",
"HELIERE",
"HELIOTT",
"HELLENA",
"HELLENE",
"HELLINE",
"HELLMUT",
"HELMUTH",
"HELODIE",
"HELOISA",
"HELOISE",
"HELOURI",
"HELWANE",
"HELYANA",
"HELYETT",
"HEMERIC",
"HEMRICK",
"HENDRIC",
"HENDRIK",
"HENDRIX",
"HENDRYK",
"HENRICH",
"HENRICK",
"HENRICO",
"HENRIKA",
"HENRINA",
"HENRIOT",
"HENRITA",
"HENRIUS",
"HENRYCK",
"HENRYKA",
"HERBERT",
"HERCULE",
"HEREITI",
"HERENUI",
"HERLAND",
"HERLINE",
"HERMAND",
"HERMANE",
"HERMANN",
"HERMANT",
"HERMINA",
"HERMINE",
"HERNANI",
"HERRADE",
"HERVINE",
"HERWANN",
"HEYTHEM",
"HICHAME",
"HICHEME",
"HICHIMA",
"HIDAYAH",
"HIDAYAT",
"HIDAYET",
"HILAIRE",
"HILARIA",
"HILARIE",
"HILARIO",
"HILLARY",
"HILYANA",
"HINANUI",
"HINATEA",
"HIRANUR",
"HISSANE",
"HISSEIN",
"HODAYFA",
"HONESTE",
"HONORAT",
"HONOREE",
"HONORIN",
"HOORAIN",
"HORACIO",
"HORATIO",
"HORLANE",
"HOSANNA",
"HOSSAIN",
"HOSSANA",
"HOSSEIN",
"HOSSINE",
"HOUAIDA",
"HOUARDA",
"HOUARIA",
"HOUCINE",
"HOULEYE",
"HOURAYE",
"HOURIDA",
"HOURIYA",
"HOUSINE",
"HOUSMAN",
"HOUSNIA",
"HOUSSAM",
"HOUSSEM",
"HOUSSEN",
"HOUSSIN",
"HOUSSNA",
"HOUSSNI",
"HOUSTON",
"HOUYAME",
"HOUZEFA",
"HOZANNA",
"HUBERTE",
"HUDAYFA",
"HUGETTE",
"HUGOLIN",
"HUMBERT",
"HUMEYRA",
"HUSEYIN",
"HUSSAIN",
"HUSSAYN",
"HUSSEIN",
"HUSSEYN",
"HUZAIFA",
"HUZEYFE",
"HYACINE",
"HYLIANA",
];
}