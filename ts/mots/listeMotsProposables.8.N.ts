export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"N'FAMADY",
"N'MAHAWA",
"NABINTOU",
"NADIEJDA",
"NADJETTE",
"NAFISSAH",
"NAGNOUMA",
"NAISSATA",
"NAMINATA",
"NAMIZATA",
"NANTENIN",
"NAOUALLE",
"NAOUELLE",
"NAPHTALI",
"NAPOLEON",
"NARCISSE",
"NARGESSE",
"NARGISSE",
"NARIMANE",
"NARIMENE",
"NARJESSE",
"NARJISSE",
"NARRIMAN",
"NASRDINE",
"NASSABIA",
"NASSEIRA",
"NASSIERA",
"NASSRINE",
"NASSURIA",
"NASTASIA",
"NASTASJA",
"NASTASYA",
"NASTAZIA",
"NATAELLE",
"NATALENE",
"NATALIJA",
"NATALINA",
"NATALINE",
"NATALINO",
"NATANAEL",
"NATANIEL",
"NATASCHA",
"NATASSIA",
"NATERCIA",
"NATHACHA",
"NATHAELE",
"NATHALAN",
"NATHALIA",
"NATHALIE",
"NATHALYA",
"NATHALYE",
"NATHANEL",
"NATHANIA",
"NATIVITE",
"NAUSICAA",
"NAZARETH",
"NAZLICAN",
"NEPHELIE",
"NEPHTALI",
"NERIMENE",
"NERLANDE",
"NESERINE",
"NESLIHAN",
"NESSAYEM",
"NESSRINE",
"NESSRYNE",
"NETHANEL",
"NICHOLAS",
"NICKOLAS",
"NICODEME",
"NICOLASA",
"NICOLETA",
"NICOLINA",
"NICOLINE",
"NICOLINO",
"NICOMEDE",
"NIKOLAOS",
"NIKOLINA",
"NIKOLOZI",
"NILOUFAR",
"NIROSHAN",
"NIRUSHAN",
"NISHANTH",
"NISSRINE",
"NITHUSAN",
"NIVETHAN",
"NOELLINE",
"NOELLISE",
"NOELLYNE",
"NOLWENNE",
"NONCIADE",
"NORADINE",
"NORBERTE",
"NORBERTO",
"NORDDINE",
"NORDINNE",
"NOREDINE",
"NORIANNE",
"NOUAMANE",
"NOUDJOUD",
"NOUFISSA",
"NOUHAILA",
"NOUHAYLA",
"NOUHEILA",
"NOUHEYLA",
"NOUJOUDE",
"NOUMIDIA",
"NOURANIA",
"NOURANNE",
"NOURCINE",
"NOURDINE",
"NOURHANE",
"NOURHENE",
"NOURIATI",
"NOURIMEN",
"NOURSANE",
"NOURSINE",
"NOUSAYBA",
"NUNZIATA",
"NURETTIN",
"NURSELIN",
"NURULLAH",
"NUSSAYBA",
];
}