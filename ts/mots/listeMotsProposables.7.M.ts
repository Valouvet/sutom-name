export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"M'BALIA",
"M'BALOU",
"M'BAREK",
"M'BARKA",
"M'BEMBA",
"M'HAMED",
"MAARTEN",
"MAAYANA",
"MAAYANE",
"MABINTY",
"MABROUK",
"MACAIRE",
"MADALEN",
"MADALIN",
"MADASSA",
"MADELIE",
"MADELON",
"MADELYN",
"MADIANA",
"MADIANE",
"MADIGAN",
"MADILYN",
"MADINAH",
"MADISON",
"MADJIDE",
"MADLEEN",
"MADLINE",
"MADLYNE",
"MADONNA",
"MADRICK",
"MADYANA",
"MADYANE",
"MADYSON",
"MAEDINE",
"MAELANE",
"MAELANN",
"MAELDAN",
"MAELEEN",
"MAELENN",
"MAELHAN",
"MAELHYS",
"MAELIAN",
"MAELICE",
"MAELICK",
"MAELINA",
"MAELINE",
"MAELISE",
"MAELISS",
"MAELLIA",
"MAELLIE",
"MAELLIS",
"MAELLYA",
"MAELLYS",
"MAELYAH",
"MAELYCE",
"MAELYNA",
"MAELYNE",
"MAELYNN",
"MAELYSE",
"MAELYSS",
"MAERONN",
"MAEVANE",
"MAEWENN",
"MAFALDA",
"MAFANTA",
"MAGALIE",
"MAGAMED",
"MAGATTE",
"MAGDALA",
"MAGELLA",
"MAGHNIA",
"MAGOMED",
"MAHALIA",
"MAHALYA",
"MAHAMAD",
"MAHAMAT",
"MAHAMED",
"MAHAMET",
"MAHAULT",
"MAHDIYA",
"MAHELIA",
"MAHELIE",
"MAHELLE",
"MAHELYA",
"MAHELYS",
"MAHFOUD",
"MAHISSA",
"MAHJOUB",
"MAHLONE",
"MAHMOUD",
"MAHNOOR",
"MAHRANE",
"MAHYSSA",
"MAIALEN",
"MAICKEL",
"MAIDINE",
"MAIGANE",
"MAILANE",
"MAILANN",
"MAILEEN",
"MAILICE",
"MAILINE",
"MAILISE",
"MAILISS",
"MAILLIE",
"MAILLYS",
"MAILYCE",
"MAILYNA",
"MAILYNE",
"MAILYNN",
"MAILYSE",
"MAILYSS",
"MAIMITI",
"MAIMUNA",
"MAIRAME",
"MAIRICK",
"MAIRONE",
"MAIRONN",
"MAISARA",
"MAISSAA",
"MAISSAE",
"MAISSAM",
"MAISSAN",
"MAISSEM",
"MAISSEN",
"MAITANE",
"MAITENA",
"MAIWEEN",
"MAIWENE",
"MAIWENN",
"MAIXENT",
"MAJORIC",
"MAJORIE",
"MAJOUBA",
"MAKBULE",
"MAKENZY",
"MAKHTAR",
"MAKLOUF",
"MAKOURA",
"MALAIKA",
"MALAURY",
"MALAYKA",
"MALCOLM",
"MALCOME",
"MALFADA",
"MALHONE",
"MALHONN",
"MALHORY",
"MALIANA",
"MALICIA",
"MALICKA",
"MALICYA",
"MALINDA",
"MALINKA",
"MALISSA",
"MALIYAH",
"MALKIEL",
"MALLIKA",
"MALLONE",
"MALLORY",
"MALONNE",
"MALORIE",
"MALOUNA",
"MALVINA",
"MALVINE",
"MALVYNA",
"MALWEEN",
"MALWENN",
"MALWINA",
"MALYANA",
"MALYCIA",
"MALYSIA",
"MALYSSA",
"MAMADOU",
"MAMASSA",
"MAMOUNA",
"MAMOUNE",
"MANAHAU",
"MANAHEL",
"MANAHIL",
"MANALLE",
"MANASSA",
"MANASSE",
"MANATEA",
"MANAVAI",
"MANDANA",
"MANDELA",
"MANDINE",
"MANDIOU",
"MANDJOU",
"MANDSON",
"MANEC'H",
"MANELLA",
"MANELLE",
"MANESSA",
"MANFRED",
"MANIAME",
"MANILLE",
"MANISHA",
"MANISSA",
"MANNAIG",
"MANOACH",
"MANOELA",
"MANOLIA",
"MANOLIE",
"MANOLIN",
"MANOLIS",
"MANOLYA",
"MANOUBI",
"MANSATA",
"MANSITA",
"MANSOUR",
"MANTHIA",
"MANUELA",
"MANUELE",
"MANUELO",
"MANUTEA",
"MANYSSA",
"MAOLINE",
"MARCEAU",
"MARCELA",
"MARCELE",
"MARCELO",
"MARCELY",
"MARCIAL",
"MARCIEN",
"MARDAYE",
"MARELLA",
"MARGAUD",
"MARGAUT",
"MARGAUX",
"MARGERY",
"MARIAMA",
"MARIAME",
"MARIAMI",
"MARIANA",
"MARIANE",
"MARIANO",
"MARIATA",
"MARIAYE",
"MARIBEL",
"MARICIA",
"MARICKA",
"MARICKE",
"MARIDZA",
"MARIEKE",
"MARIELA",
"MARIELE",
"MARIEME",
"MARIETA",
"MARIEVA",
"MARIEVE",
"MARIJAN",
"MARIJKE",
"MARILIA",
"MARILIE",
"MARILIS",
"MARILOU",
"MARILYN",
"MARILYS",
"MARINHA",
"MARINHO",
"MARINKA",
"MARINKO",
"MARINNA",
"MARINNE",
"MARIOLA",
"MARISCA",
"MARISKA",
"MARISOL",
"MARISSA",
"MARITHE",
"MARITIE",
"MARITXU",
"MARITZA",
"MARIUSZ",
"MARIVEL",
"MARIYAH",
"MARIYAM",
"MARJANA",
"MARJANE",
"MARJORY",
"MARLEEN",
"MARLENA",
"MARLENE",
"MARLINE",
"MARLISE",
"MARLONE",
"MARLONN",
"MARLOWE",
"MARLOWN",
"MARLYNE",
"MARLYSE",
"MAROINE",
"MAROUAN",
"MAROUEN",
"MARTIAL",
"MARTINA",
"MARTINE",
"MARTINO",
"MARTINS",
"MARTYNA",
"MARTYNE",
"MARUSKA",
"MARVEEN",
"MARVENS",
"MARVINA",
"MARVINE",
"MARVING",
"MARVINN",
"MARVINS",
"MARVYNE",
"MARVYNN",
"MARWANE",
"MARWANN",
"MARWENE",
"MARYAMA",
"MARYAME",
"MARYANA",
"MARYANE",
"MARYANN",
"MARYBEL",
"MARYEME",
"MARYEVE",
"MARYJAN",
"MARYLEE",
"MARYLEN",
"MARYLIA",
"MARYLIE",
"MARYLIN",
"MARYLIS",
"MARYLOU",
"MARYLYN",
"MARYNNE",
"MARYSIA",
"MARYSKA",
"MARYSOL",
"MARYSSA",
"MARYVON",
"MARZOUK",
"MASSARA",
"MASSENA",
"MASSENI",
"MASSIGA",
"MASSIKA",
"MASSILA",
"MASSIMO",
"MASSINA",
"MASSINE",
"MASSIRA",
"MASSIRE",
"MASSITA",
"MASSIVA",
"MASSOUD",
"MASSYLE",
"MATENIN",
"MATENZO",
"MATERNE",
"MATEUSZ",
"MATHEIS",
"MATHEOS",
"MATHEUS",
"MATHEWS",
"MATHEYS",
"MATHIAS",
"MATHIEU",
"MATHIEW",
"MATHIOU",
"MATHISS",
"MATHITA",
"MATHURA",
"MATHYAS",
"MATHYEU",
"MATHYSS",
"MATILDA",
"MATILDE",
"MATILIN",
"MATILIO",
"MATISSE",
"MATONDO",
"MATTEIS",
"MATTEWS",
"MATTHEO",
"MATTHEW",
"MATTHIS",
"MATTHYS",
"MATTIAS",
"MATTIEU",
"MATTIEW",
"MATTYAS",
"MATURIN",
"MATYLDA",
"MATYLDE",
"MATYLIO",
"MATYSSE",
"MAURADE",
"MAURANE",
"MAUREEN",
"MAURENA",
"MAURENE",
"MAURICE",
"MAURINE",
"MAURISE",
"MAURYNE",
"MAVRICK",
"MAWUENA",
"MAXANCE",
"MAXENCE",
"MAXETTE",
"MAXIMIN",
"MAXIMUS",
"MAXWELL",
"MAYALEN",
"MAYANNA",
"MAYANNE",
"MAYDINE",
"MAYELLE",
"MAYERON",
"MAYEULE",
"MAYGANE",
"MAYLANA",
"MAYLANE",
"MAYLANN",
"MAYLEEN",
"MAYLENE",
"MAYLICE",
"MAYLINA",
"MAYLINE",
"MAYLING",
"MAYLINH",
"MAYLINN",
"MAYLISE",
"MAYLISS",
"MAYLLIE",
"MAYLLIS",
"MAYLONE",
"MAYLONN",
"MAYMANA",
"MAYMUNA",
"MAYRICK",
"MAYRINE",
"MAYRONE",
"MAYRONN",
"MAYSANE",
"MAYSARA",
"MAYSENE",
"MAYSOUN",
"MAYSSAA",
"MAYSSAE",
"MAYSSAM",
"MAYSSAN",
"MAYSSEM",
"MAYSSEN",
"MAYSSON",
"MAYURAN",
"MAYWENN",
"MAZOUZA",
"MEAGHAN",
"MEBAREK",
"MEBARKA",
"MEBROUK",
"MEDERIC",
"MEDERIK",
"MEDOUNE",
"MEDRICK",
"MEGANNE",
"MEGGANE",
"MEGHANE",
"MEGHANN",
"MEHAMED",
"MEHDINE",
"MEHREEN",
"MEIGGIE",
"MEILANE",
"MEILINE",
"MEILING",
"MEILYNE",
"MEILYNN",
"MEINRAD",
"MEISSAN",
"MELAHAT",
"MELAINE",
"MELANIA",
"MELANIE",
"MELANYE",
"MELCHOR",
"MELDINE",
"MELEANE",
"MELIANA",
"MELIANE",
"MELIANI",
"MELICIA",
"MELINAY",
"MELINDA",
"MELINEE",
"MELINNA",
"MELISSA",
"MELISSE",
"MELIYAH",
"MELKIOR",
"MELLILA",
"MELLINA",
"MELLINE",
"MELLONY",
"MELLYNA",
"MELLYNE",
"MELOANE",
"MELODIA",
"MELODIE",
"MELODYE",
"MELONIE",
"MELOUKA",
"MELRICK",
"MELVEEN",
"MELVINA",
"MELVINE",
"MELVINN",
"MELVYNA",
"MELVYNE",
"MELVYNN",
"MELWANN",
"MELWENN",
"MELYANA",
"MELYANE",
"MELYCIA",
"MELYNDA",
"MELYNNA",
"MELYSSA",
"MELYSSE",
"MEMOUNA",
"MEMPHIS",
"MENAHEM",
"MENEKSE",
"MENELIK",
"MENELLE",
"MENESSA",
"MENISSA",
"MENOUAR",
"MENOUER",
"MENOUHA",
"MENPHIS",
"MENPHYS",
"MENSOUR",
"MERIAMA",
"MERIAME",
"MERIEME",
"MERIYEM",
"MERLENE",
"MERLINE",
"MERLYNE",
"MEROINE",
"MEROUAN",
"MERRICK",
"MERVEIL",
"MERWANE",
"MERWANN",
"MERYAME",
"MERYEME",
"MERYLIE",
"MERYLLE",
"MERZAKA",
"MERZOUK",
"MESCHAC",
"MESMINE",
"MESSAAD",
"MESSALI",
"MESSIKA",
"MESSINA",
"MESSONE",
"MESTAFA",
"METEHAN",
"METISSE",
"MEVLANA",
"MEVLUDE",
"MEVRICK",
"MEYDINE",
"MEYGANE",
"MEYLANE",
"MEYLANN",
"MEYLEEN",
"MEYLINA",
"MEYLINE",
"MEYRIAM",
"MEYRIEM",
"MEYRONE",
"MEYRONN",
"MEYSSAN",
"MEYSSEM",
"MEYSSEN",
"MEYSSON",
"MEZIANE",
"MEZYANE",
"MICAELA",
"MICHAEL",
"MICHEAL",
"MICHELA",
"MICHELE",
"MICIPSA",
"MICKAEL",
"MICKAIL",
"MIESZKO",
"MIGUELA",
"MIGUELE",
"MIHAELA",
"MIHAILO",
"MIHAJLO",
"MIKAELA",
"MIKAELE",
"MIKAHIL",
"MIKAYIL",
"MIKHAEL",
"MIKHAIL",
"MIKOLAJ",
"MILAGRO",
"MILAINE",
"MILANDA",
"MILANKA",
"MILANNA",
"MILDRED",
"MILEENA",
"MILENKO",
"MILENZO",
"MILHANE",
"MILHANN",
"MILIANA",
"MILIANE",
"MILIANI",
"MILIANN",
"MILIANO",
"MILINDA",
"MILISSA",
"MILJANA",
"MILLENA",
"MILLIAN",
"MILORAD",
"MILOUDA",
"MILOUDE",
"MILOUDI",
"MILOVAN",
"MILYANA",
"MILYANN",
"MILYANO",
"MIMOUNA",
"MIMOUNE",
"MIMOUNT",
"MINAHIL",
"MINERVA",
"MINERVE",
"MINETTE",
"MIODRAG",
"MIRABEL",
"MIRACLE",
"MIRADIE",
"MIRADJI",
"MIRALDA",
"MIRANDA",
"MIRANDE",
"MIRANDO",
"MIRELLA",
"MIRELLE",
"MIRETTE",
"MIRIAME",
"MIRIANA",
"MIRIANE",
"MIRIEME",
"MIRJANA",
"MIRLENE",
"MIRSADA",
"MIRYAME",
"MIRYANA",
"MISHEEL",
"MISLINA",
"MISSOUM",
"MISTRAL",
"MITCHEL",
"MITSUKO",
"MOCKTAR",
"MODERAN",
"MODESTA",
"MODESTE",
"MODESTO",
"MODESTY",
"MOHAMAD",
"MOHAMED",
"MOHAMET",
"MOHANAD",
"MOHANED",
"MOHCENE",
"MOHCINE",
"MOHSINE",
"MOHSSEN",
"MOHSSIN",
"MOKHTAR",
"MOKRANE",
"MOKTHAR",
"MOLIERE",
"MONDANE",
"MONDHER",
"MONELLE",
"MONETTE",
"MONIQUA",
"MONIQUE",
"MONSOUR",
"MONSSEF",
"MONTANA",
"MORANNE",
"MORGANA",
"MORGANE",
"MORGANN",
"MORGHAN",
"MORIANE",
"MORJANA",
"MORJANE",
"MORLAYE",
"MORTADA",
"MORWENA",
"MORWENN",
"MOSSAAB",
"MOSTAFA",
"MOSTEFA",
"MOUAYAD",
"MOUAYED",
"MOUBINE",
"MOUCHKA",
"MOUCTAR",
"MOUDJIB",
"MOUFIDA",
"MOUHAND",
"MOUHOUB",
"MOUHSIN",
"MOUKTAR",
"MOULAYE",
"MOULOUD",
"MOUMINA",
"MOUMINE",
"MOUNAIM",
"MOUNAYA",
"MOUNCEF",
"MOUNDIR",
"MOUNERA",
"MOUNINA",
"MOUNIRA",
"MOUNIRE",
"MOUNIYA",
"MOUNSEF",
"MOUNSIF",
"MOURADE",
"MOURRAD",
"MOUSLIM",
"MOUSSAB",
"MOUSSIA",
"MOUSSOU",
"MUAMMER",
"MUBERRA",
"MUCAHID",
"MUCAHIT",
"MUHAMAD",
"MUHAMED",
"MUHAMET",
"MUHAREM",
"MUNTAHA",
"MURIANE",
"MURIELE",
"MURRIEL",
"MURTAZA",
"MUSTAFA",
"MYCHAEL",
"MYCKAEL",
"MYLAINE",
"MYLANNA",
"MYLEINE",
"MYLENNA",
"MYLHANE",
"MYLHANN",
"MYLIANA",
"MYLIANE",
"MYLIANO",
"MYRANDA",
"MYRELLA",
"MYRETTE",
"MYRIAMA",
"MYRIAME",
"MYRIANA",
"MYRIANE",
"MYRIEME",
"MYRLENE",
"MYRTILE",
];
}