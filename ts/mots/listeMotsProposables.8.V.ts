export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"VAILLANT",
"VAITIARE",
"VALANTIN",
"VALDEMAR",
"VALENCIA",
"VALENTIM",
"VALENTIN",
"VALENTYN",
"VALERIAN",
"VALERIEN",
"VALERINE",
"VANGELIS",
"VANNESSA",
"VASILIJE",
"VASSILIA",
"VASSILIS",
"VENANCIO",
"VENDELIN",
"VENERAND",
"VENISSIA",
"VENUSSIA",
"VERGINIA",
"VERLAINE",
"VERONICA",
"VERONIKA",
"VIANETTE",
"VICHENZO",
"VICTOIRE",
"VICTORIA",
"VICTORIN",
"VICTORIO",
"VICTORYA",
"VIKTORIA",
"VINCENCE",
"VINCENNE",
"VINCENTA",
"VINCENTE",
"VINCENZA",
"VINCENZO",
"VINCIANE",
"VINCILIA",
"VINICIUS",
"VIOLAINE",
"VIOLANDA",
"VIOLETTA",
"VIOLETTE",
"VIRGILIA",
"VIRGILIO",
"VIRGINIA",
"VIRGINIE",
"VIRGINIO",
"VIRTUDES",
"VITALIEN",
"VITALINA",
"VITALINE",
"VITHURAN",
"VITHUSAN",
"VITHUSHA",
"VITTORIA",
"VITTORIO",
"VIVIANNE",
"VIVIENNE",
"VLADIMIR",
"VOLTAIRE",
"VYCTORIA",
];
}