export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"JAAFAR",
"JABIER",
"JABRAN",
"JABRIL",
"JACKIE",
"JACKYE",
"JACOPO",
"JACQUE",
"JACQUI",
"JACQUY",
"JADDEN",
"JADENE",
"JADENN",
"JADHEN",
"JAEDEN",
"JAELLE",
"JAELYN",
"JAELYS",
"JAFFAR",
"JAGODA",
"JAHDEN",
"JAHEEM",
"JAHEIM",
"JAHIDA",
"JAHIEM",
"JAHINA",
"JAHLAN",
"JAHLEN",
"JAHLIA",
"JAHLIL",
"JAHLYA",
"JAHLYS",
"JAHMAL",
"JAHMEL",
"JAHNIS",
"JAHNYS",
"JAHROD",
"JAHRON",
"JAHWAN",
"JAHWEL",
"JAHWEN",
"JAHYAH",
"JAHYAN",
"JAHYNA",
"JAIDEN",
"JAILAN",
"JAILYS",
"JAIMIE",
"JAIRON",
"JAISON",
"JAKSON",
"JALALE",
"JALANE",
"JALEEL",
"JALILA",
"JALILE",
"JALINA",
"JALIYA",
"JALLAL",
"JALLEL",
"JALLIL",
"JALYAH",
"JAMAAL",
"JAMAEL",
"JAMALE",
"JAMALL",
"JAMELA",
"JAMELE",
"JAMICE",
"JAMILA",
"JAMILY",
"JAMINA",
"JAMMEL",
"JAMMES",
"JAMYLA",
"JANAEL",
"JANAIS",
"JANANE",
"JANANI",
"JANATE",
"JANAYA",
"JANELE",
"JANELL",
"JANICE",
"JANICK",
"JANINA",
"JANINE",
"JANISE",
"JANITA",
"JANNAH",
"JANNAT",
"JANNET",
"JANNIC",
"JANNIE",
"JANNIK",
"JANNIS",
"JANNOT",
"JANUSZ",
"JANYCE",
"JANYNE",
"JAOIDE",
"JAOUAD",
"JAOUED",
"JAOUEN",
"JAPHET",
"JAQUES",
"JARDEL",
"JARELL",
"JARODE",
"JAROLD",
"JARROD",
"JARVIS",
"JASMEE",
"JASMIN",
"JASMYN",
"JASONE",
"JASPER",
"JASSEM",
"JASSER",
"JASSIM",
"JASSIR",
"JASSON",
"JASSYM",
"JAUFRE",
"JAUREL",
"JAURES",
"JAURIS",
"JAVIEL",
"JAVIER",
"JAWADE",
"JAWDAN",
"JAWHAR",
"JAYANA",
"JAYANE",
"JAYANI",
"JAYANN",
"JAYCEE",
"JAYDAN",
"JAYDEN",
"JAYDON",
"JAYHAN",
"JAYLAN",
"JAYLEE",
"JAYLEN",
"JAYLIE",
"JAYLIS",
"JAYLON",
"JAYMES",
"JAYMIE",
"JAYRON",
"JAYSON",
"JAYWEN",
"JAYZON",
"JEAMES",
"JEANIE",
"JEANNA",
"JEANNE",
"JEANNY",
"JEANOT",
"JEANTY",
"JEASON",
"JEBRIL",
"JEEVAN",
"JEFFRY",
"JEHANE",
"JEHANN",
"JEHIEL",
"JELANI",
"JELANY",
"JELENA",
"JELICA",
"JELILA",
"JELINA",
"JEMAEL",
"JEMILA",
"JEMIMA",
"JEMINA",
"JEMUEL",
"JENAEL",
"JENANE",
"JENAYA",
"JENAYE",
"JENINE",
"JENITA",
"JENNAH",
"JENNAT",
"JENNIE",
"JENOFA",
"JENSEN",
"JENSON",
"JEPHTE",
"JEREMI",
"JEREMY",
"JERICK",
"JERIEL",
"JEROEN",
"JEROME",
"JERSEY",
"JERSON",
"JESICA",
"JESONE",
"JESSEE",
"JESSEM",
"JESSEN",
"JESSEY",
"JESSIA",
"JESSIE",
"JESSIM",
"JESSON",
"JESSYE",
"JESSYM",
"JESULA",
"JETHRO",
"JEYDEN",
"JEYLAN",
"JEYSON",
"JHONNY",
"JIANNI",
"JIANNY",
"JIBRAN",
"JIBRIL",
"JIBRYL",
"JIHADE",
"JIHANE",
"JIHENE",
"JILALI",
"JILANE",
"JILANI",
"JILDAZ",
"JILIAN",
"JILYAN",
"JIMENA",
"JIMMIE",
"JINANE",
"JINENE",
"JIULIA",
"JOAKIM",
"JOAKIN",
"JOAKYM",
"JOANEL",
"JOANES",
"JOANIE",
"JOANIS",
"JOANNA",
"JOANNE",
"JOANNI",
"JOANNY",
"JOCHEN",
"JODDIE",
"JOELIE",
"JOELLA",
"JOELLE",
"JOELLY",
"JOEVIN",
"JOFFRE",
"JOFREY",
"JOHANA",
"JOHANE",
"JOHANN",
"JOHANY",
"JOHARA",
"JOHARY",
"JOHLAN",
"JOHNAS",
"JOHNNY",
"JOHSUA",
"JOLANE",
"JOLANN",
"JOLEEN",
"JOLENE",
"JOLHAN",
"JOLIAN",
"JOLINA",
"JOLINE",
"JOLYNE",
"JOMANA",
"JONAEL",
"JONASS",
"JONASZ",
"JONNHY",
"JORANE",
"JORANN",
"JORDAN",
"JORDIE",
"JORGEN",
"JORHAN",
"JORIAN",
"JORICE",
"JORICK",
"JORLAN",
"JORRIS",
"JORRYS",
"JORYAN",
"JORYCE",
"JOSANE",
"JOSEBA",
"JOSEFA",
"JOSELY",
"JOSEPH",
"JOSETE",
"JOSHUA",
"JOSHUE",
"JOSIAH",
"JOSIAN",
"JOSIAS",
"JOSICK",
"JOSINE",
"JOSITA",
"JOSLIN",
"JOSLYN",
"JOSSIA",
"JOSSIE",
"JOSSUA",
"JOSUAH",
"JOSUAN",
"JOSUHA",
"JOSYAN",
"JOSYNE",
"JOTHAM",
"JOUANA",
"JOUDIA",
"JOUDYA",
"JOULIA",
"JOULYA",
"JOVANA",
"JOVANE",
"JOVANI",
"JOVANN",
"JOVANY",
"JOVICA",
"JOZEFA",
"JOZSEF",
"JUANNA",
"JUDITE",
"JUDITH",
"JUDSON",
"JULIAN",
"JULIDE",
"JULIEN",
"JULIET",
"JULIJA",
"JULINA",
"JULINE",
"JULITA",
"JULIUS",
"JULLIA",
"JULUAN",
"JULYAN",
"JULYEN",
"JULYNE",
"JUMANA",
"JUNAID",
"JUNAYD",
"JUNEYD",
"JUNIEN",
"JUNIOR",
"JURGEN",
"JURIEN",
"JUSTIN",
"JUSTYN",
"JYHANE",
];
}