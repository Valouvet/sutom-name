export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"YANNAELLE",
"YASSAMINE",
"YASSIMINA",
"YEHOUDITH",
"YEKOUTIEL",
"YOUGOUDOU",
"YOUNOUSSA",
"YOUNOUSSE",
"YOUSSOUFA",
"YOUSSOUFI",
"YOUSSOUPH",
"YUNUSEMRE",
"YVONNETTE",
];
}