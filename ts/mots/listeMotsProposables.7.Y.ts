export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"YAACOUB",
"YAAKOUB",
"YACOUBA",
"YADWIGA",
"YAGOUBA",
"YAHCINE",
"YAKOUBA",
"YAKOUTA",
"YAKOUTE",
"YAMADOU",
"YAMANDA",
"YAMOUNA",
"YANCOUB",
"YANELLE",
"YANESSA",
"YANISSA",
"YANISSE",
"YANNAEL",
"YANNECK",
"YANNICE",
"YANNICK",
"YANNISS",
"YANOURA",
"YAQUINE",
"YAROUBA",
"YASEMIN",
"YASMEEN",
"YASMINA",
"YASMINE",
"YASRINE",
"YASSIME",
"YASSINA",
"YASSINE",
"YASSINI",
"YASSIRA",
"YASSIRE",
"YASSMIN",
"YAZIDOU",
"YBRAHIM",
"YDRISSA",
"YEDIDIA",
"YEHOUDA",
"YELENNA",
"YELLANA",
"YELLENA",
"YESMINA",
"YESMINE",
"YESSINE",
"YIANNIS",
"YLIANNA",
"YLIESSE",
"YLLIANA",
"YNAELLE",
"YOACHIM",
"YOANNIS",
"YOHANAN",
"YOHANES",
"YOHANNA",
"YOHANNE",
"YOHEVED",
"YOLAINE",
"YOLANDA",
"YOLANDE",
"YOLEINE",
"YOLENNE",
"YOLETTE",
"YONATAN",
"YONNICK",
"YORRICK",
"YOSSEPH",
"YOUNECE",
"YOUNESE",
"YOUNESS",
"YOUNISS",
"YOUNNES",
"YOUNOUS",
"YOUNSSE",
"YOURICK",
"YOUSOUF",
"YOUSRAH",
"YOUSRAT",
"YOUSROI",
"YOUSSAF",
"YOUSSEF",
"YOUSSEM",
"YOUSSIF",
"YOUSSOF",
"YOUSSOU",
"YOUSSRA",
"YOUSSRI",
"YOUSSUF",
"YOUSTON",
"YOUWENN",
"YSABEAU",
"YSALINE",
"YSAMBRE",
"YSANDRE",
"YSELINE",
"YSOLINE",
"YULIANA",
"YUSSOUF",
"YVANNAH",
"YVELINE",
"YVELISE",
"YVELYNE",
"YVELYSE",
"YVENSON",
"YVONICK",
"YVONNET",
"YVONNIC",
"YVONNIG",
"YVONNIK",
"YZALINE",
];
}