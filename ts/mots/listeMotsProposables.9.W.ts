export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"WADISLAWA",
"WADYSLAWA",
"WALLERAND",
"WANDRILLE",
"WENCESLAS",
"WENDELINE",
"WIELFRIED",
"WILLFRIED",
"WILLIAM'S",
"WILLIBALD",
"WLADISLAS",
"WLADISLAW",
"WLADYSLAS",
"WLADYSLAW",
];
}