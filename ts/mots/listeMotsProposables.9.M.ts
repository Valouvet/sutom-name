export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"MACINISSA",
"MACKENSON",
"MACKENZIE",
"MADDALENA",
"MADELAINE",
"MADELEINE",
"MADISONNE",
"MADISSONE",
"MADOUSSOU",
"MADYSSONE",
"MAELLYSSE",
"MAGDALENA",
"MAGDALENE",
"MAGDALINA",
"MAGDELENA",
"MAGDELINE",
"MAGUELONE",
"MAHAMADOU",
"MAHAMODOU",
"MAHASSINE",
"MAHDJOUBA",
"MAHEDDINE",
"MAHEIDINE",
"MAHIDDINE",
"MAHIEDINE",
"MAHMOUDOU",
"MAISSOUNE",
"MAJDELINE",
"MAJDOLINE",
"MALLAURIE",
"MAMADOUBA",
"MANDARINE",
"MANDOLINE",
"MANHATTAN",
"MANNUELLA",
"MANOUCHKA",
"MANSOURIA",
"MANUELITA",
"MARANATHA",
"MARAVILHA",
"MARC'ANTO",
"MARCELINA",
"MARCELINE",
"MARCELINO",
"MARCELLIA",
"MARCELLIN",
"MARCELLUS",
"MARCELYNE",
"MARCIENNE",
"MARCUCCIA",
"MARDOCHEE",
"MARGALITH",
"MARGARETA",
"MARGARETE",
"MARGARETH",
"MARGARETT",
"MARGARIDA",
"MARGARITA",
"MARIALINE",
"MARIANICK",
"MARIANNIC",
"MARIANNIK",
"MARIBELLE",
"MARILAINE",
"MARILEINE",
"MARINELLA",
"MARINELLE",
"MARINETTE",
"MARISETTE",
"MARIUCCIA",
"MARIVONNE",
"MARIYAMOU",
"MARJOLENE",
"MARJOLINE",
"MARJORINE",
"MARLYATOU",
"MAROUCHKA",
"MAROUSSIA",
"MARTINIEN",
"MARUSCHKA",
"MARVELOUS",
"MARYALINE",
"MARYANICK",
"MARYLAINE",
"MARYLAURE",
"MARYLEINE",
"MARYNETTE",
"MARYSETTE",
"MARYVETTE",
"MARYVONNE",
"MASINISSA",
"MASSANDJE",
"MASSINISA",
"MATHURINE",
"MATHUSHAN",
"MATIGUIDA",
"MATTHIEUX",
"MAURIANNE",
"MAURILLIA",
"MAXIMILIA",
"MAXIMILIE",
"MAYASSINE",
"MAYBELINE",
"MAYMOUNAH",
"MAYSSOUNE",
"MECHTILDE",
"MEDERIQUE",
"MEHMETALI",
"MEHMETCAN",
"MEHMETHAN",
"MELISANDE",
"MELISENDE",
"MELISSANE",
"MENEHOULD",
"MERITXELL",
"MERVEILLE",
"MESSALINE",
"MESSAOUDA",
"MICHAELLA",
"MICHAELLE",
"MICHALINA",
"MICHELINA",
"MICHELINE",
"MICKAELLA",
"MICKAELLE",
"MIECISLAS",
"MIECISLAW",
"MIECYSLAW",
"MIGUELITO",
"MILLICENT",
"MIMOSETTE",
"MIRABELLA",
"MIRABELLE",
"MIRENTCHU",
"MIROSLAVA",
"MODESTINE",
"MOHAMADOU",
"MOINAECHA",
"MOKHTARIA",
"MONSERRAT",
"MONTASSAR",
"MONTASSER",
"MORDEKHAI",
"MORICETTE",
"MORRIGANE",
"MOUDJAHID",
"MOUDJIBOU",
"MOUHAMADI",
"MOUHAMMAD",
"MOUHAMMED",
"MOUHSSINE",
"MOUMADJAD",
"MOUNIAPIN",
"MOUSTAKIM",
"MOUSTAPHA",
"MOUSTOIFA",
];
}