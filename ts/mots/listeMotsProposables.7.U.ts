export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"UGOLINE",
"UGUETTE",
"ULDERIC",
"ULDRICH",
"ULDRICK",
"ULYSSES",
"ULYSSIA",
"UMBERTO",
"UMMAHAN",
"UMMUHAN",
"UMUTCAN",
"URBAINE",
"URBANIE",
"URIELLE",
"URSULLA",
];
}