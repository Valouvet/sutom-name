export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"JACQUELIN",
"JACQUETTE",
"JAHNAELLE",
"JAMELDINE",
"JAQUELINE",
"JEANNELLE",
"JEANNETTE",
"JEANNILLE",
"JEANNIQUE",
"JEANNOTTE",
"JEFFERSON",
"JENNIFFER",
"JENNYPHER",
"JESABELLE",
"JESSALYNN",
"JEZABELLE",
"JHONATHAN",
"JOCELAINE",
"JOFFRETTE",
"JOHANNICK",
"JOHNATHAN",
"JOHNATTAN",
"JOINVILLE",
"JONATHANE",
"JONATHANN",
"JONNATHAN",
"JOSCELINE",
"JOSCELYNE",
"JOSELAINE",
"JOSEPHINA",
"JOSEPHINE",
"JOSSELINE",
"JOSSELYNE",
"JOSSERAND",
"JOUWAYRIA",
"JUDICAELE",
"JUDICKAEL",
"JUILLETTE",
"JULLIETTE",
"JUSTINIEN",
"JUWAYRIYA",
];
}