export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"TABARA",
"TABATA",
"TABITA",
"TADDEO",
"TADEUS",
"TADEUZ",
"TADZIO",
"TAFARI",
"TAHANI",
"TAHARA",
"TAHERA",
"TAHIEL",
"TAHINA",
"TAHIRA",
"TAHIRY",
"TAHITI",
"TAHLIA",
"TAHLYA",
"TAHNEE",
"TAHSIN",
"TAHYNA",
"TAHYRA",
"TAHYSS",
"TAIANA",
"TAICHA",
"TAILAN",
"TAILYS",
"TAIRIC",
"TAIRIK",
"TAIRON",
"TAIROU",
"TAIRYS",
"TAISHA",
"TAISSA",
"TAISSY",
"TAKINE",
"TAKLIT",
"TAKOUA",
"TAKUMA",
"TAKUMI",
"TALHIA",
"TALHYA",
"TALIAH",
"TALIBE",
"TALINA",
"TALINE",
"TALITA",
"TALIYA",
"TALLAL",
"TALLIA",
"TALLYA",
"TALYAH",
"TALYNA",
"TALYNE",
"TAMANI",
"TAMARA",
"TAMAYA",
"TAMERA",
"TAMIKA",
"TAMILA",
"TAMIME",
"TAMINA",
"TAMIRA",
"TAMLYN",
"TAMSIN",
"TAMSIR",
"TAMYRA",
"TANAEL",
"TANAIS",
"TANAYA",
"TANGUI",
"TANGUY",
"TANINA",
"TANITA",
"TANIYA",
"TANYAH",
"TAOFIK",
"TAOUBA",
"TAOUES",
"TARECK",
"TARICK",
"TARIKE",
"TARKAN",
"TARYLL",
"TARZAN",
"TASLIM",
"TASNIM",
"TASNYM",
"TASSIA",
"TAUFIK",
"TAWFIK",
"TAWFIQ",
"TAWHID",
"TAYANA",
"TAYANN",
"TAYCIR",
"TAYFUN",
"TAYFUR",
"TAYLAN",
"TAYLER",
"TAYLON",
"TAYLOR",
"TAYRIC",
"TAYRIK",
"TAYRIS",
"TAYRON",
"TAYSHA",
"TAYSIR",
"TAYSON",
"TAYSSA",
"TAYYAB",
"TAYYIB",
"TAYYIP",
"TAYZON",
"TCHENG",
"TEDDIE",
"TEEDJY",
"TEEYAH",
"TEGWEN",
"TEHANI",
"TEHILA",
"TEHINA",
"TEISSA",
"TELDJA",
"TELIAU",
"TELLIA",
"TEMEIO",
"TENGIS",
"TENZIN",
"TEODOR",
"TEOFIL",
"TEOMAN",
"TEOTIM",
"TERENA",
"TERESA",
"TERESE",
"TEREZA",
"TERRIE",
"TESLIM",
"TESNIM",
"TESNYM",
"TESSAH",
"TESSIA",
"TESSIE",
"TESSIO",
"TESSYA",
"TETSUO",
"TEVFIK",
"TEWFIK",
"TEXANE",
"TEYANA",
"TEYSSA",
"THADDE",
"THADEE",
"THAINA",
"THAIRA",
"THAISS",
"THALIA",
"THALIE",
"THALYA",
"THALYS",
"THAMAR",
"THANIA",
"THANYA",
"THARUN",
"THAYLA",
"THAYNA",
"THAYRA",
"THEANA",
"THEANE",
"THECLA",
"THECLE",
"THEDDY",
"THEKLA",
"THELIA",
"THELIO",
"THELMA",
"THELOR",
"THELYA",
"THELYO",
"THEMIS",
"THEMYS",
"THERRY",
"THESEE",
"THESSA",
"THESSY",
"THEVAN",
"THIABA",
"THIAGO",
"THIANA",
"THIANE",
"THIARA",
"THIBAU",
"THIERY",
"THILDA",
"THILDY",
"THILIO",
"THIMEO",
"THIORO",
"THIVEN",
"THIVYA",
"THIYYA",
"THOMAS",
"THOMMY",
"THYAGO",
"THYANA",
"THYLAN",
"THYLER",
"THYLIA",
"THYLIO",
"THYMEA",
"THYMEO",
"THYRON",
"TIANAH",
"TIANNA",
"TIBALT",
"TIBAUD",
"TIBAUT",
"TIBERE",
"TIBILE",
"TIDIAN",
"TIDJAN",
"TIDYAN",
"TIENZO",
"TIERNO",
"TIERRY",
"TIFANI",
"TIFANY",
"TIFENE",
"TIFENN",
"TIFFEN",
"TIGANE",
"TIGANN",
"TIGRAN",
"TIHANA",
"TIJANA",
"TIJANE",
"TIJANI",
"TIJANY",
"TILIAN",
"TILILA",
"TILLIA",
"TILLIE",
"TILLIO",
"TILYAN",
"TIMAEL",
"TIMAHE",
"TIMEHO",
"TIMHEO",
"TIMOTE",
"TIMOTI",
"TIMOTY",
"TIMOUR",
"TINAEL",
"TINAIG",
"TINAYA",
"TIPHEN",
"TIRTSA",
"TISSAM",
"TISSEM",
"TISSIA",
"TISSYA",
"TITIEN",
"TITOAN",
"TITUAN",
"TIWENN",
"TIYANA",
"TIZIRI",
"TOANUI",
"TOBIAS",
"TOBYAS",
"TOINON",
"TOMASA",
"TOMASO",
"TOMASZ",
"TOMMIE",
"TOMOKI",
"TONINA",
"TONINO",
"TONYNO",
"TOPRAK",
"TORKIA",
"TORRES",
"TOSCAN",
"TOUATI",
"TOUFEK",
"TOUFIC",
"TOUFIK",
"TOUFIR",
"TOUMIA",
"TOUNES",
"TOURIA",
"TOURYA",
"TRACEY",
"TRACIE",
"TRAIAN",
"TRAICY",
"TRAVIS",
"TRAVYS",
"TRECIA",
"TREICY",
"TRESOR",
"TRESSY",
"TREVIS",
"TREVOR",
"TREVYS",
"TRIANA",
"TRICHA",
"TRICIA",
"TRICYA",
"TRISHA",
"TRIXIE",
"TRUONG",
"TRYCIA",
"TRYSHA",
"TSILIA",
"TSILLA",
"TSIORY",
"TSIVIA",
"TUDUAL",
"TUDWAL",
"TUGRUL",
"TULINE",
"TULLIA",
"TULLIO",
"TUNCAY",
"TURGAY",
"TURGUT",
"TURHAN",
"TURKAN",
"TURKER",
"TURKIA",
"TWIGGY",
"TXOMIN",
"TYANAH",
"TYANNA",
"TYBALT",
"TYDIAN",
"TYFENE",
"TYFENN",
"TYFFEN",
"TYGANE",
"TYHANA",
"TYLANE",
"TYLANN",
"TYLIAM",
"TYLIAN",
"TYLLER",
"TYLLIA",
"TYMAEL",
"TYMEHO",
"TYMOTE",
"TYNAEL",
"TYPHEN",
"TYREEK",
"TYRELL",
"TYRESE",
"TYRICK",
"TYRONE",
"TYRONN",
"TYSSEM",
"TYSSIA",
"TYWENN",
];
}