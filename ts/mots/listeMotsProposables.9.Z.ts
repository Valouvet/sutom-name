export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"ZACCHARIA",
"ZACCHARIE",
"ZACHARIAH",
"ZACHARIAS",
"ZACHARIYA",
"ZACKARIYA",
"ZAKARIYYA",
"ZAOUDJATI",
"ZDZISLAWA",
"ZEPHIRINE",
"ZEPHYRINE",
"ZINEDDINE",
];
}