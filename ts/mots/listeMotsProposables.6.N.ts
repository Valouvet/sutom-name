export default class ListeMotsProposables {
public static readonly Dictionnaire: Array<string> = [
"N'DEYE",
"N'FALY",
"N'GOLO",
"N'GONE",
"N'NENE",
"N'RICK",
"NAAIMA",
"NAAMAN",
"NABEEL",
"NABELA",
"NABIHA",
"NABILA",
"NABILE",
"NABYLA",
"NABYLE",
"NACERA",
"NACERE",
"NACEUR",
"NACIMA",
"NACIME",
"NACIRA",
"NACIYE",
"NADEEM",
"NADEGE",
"NADEJE",
"NADERA",
"NADHEM",
"NADHIA",
"NADHIR",
"NADIFA",
"NADIHA",
"NADILA",
"NADIME",
"NADINA",
"NADINE",
"NADIRA",
"NADIRE",
"NADIYA",
"NADIYE",
"NADJAD",
"NADJAH",
"NADJAT",
"NADJET",
"NADJIA",
"NADJIB",
"NADJID",
"NADJIM",
"NADJLA",
"NADJMA",
"NADJWA",
"NADYNE",
"NAELAN",
"NAELIA",
"NAELIE",
"NAELLA",
"NAELLE",
"NAELLY",
"NAELYA",
"NAELYS",
"NAEMIE",
"NAEVIA",
"NAFIDA",
"NAFILA",
"NAFISA",
"NAFIYE",
"NAGETE",
"NAGIBA",
"NAGIMA",
"NAGUIB",
"NAHEEL",
"NAHELA",
"NAHELE",
"NAHEMA",
"NAHIDA",
"NAHIDE",
"NAHILA",
"NAHIMA",
"NAHIYA",
"NAHLAN",
"NAHMAN",
"NAHOMI",
"NAHOMY",
"NAHOUA",
"NAHOUM",
"NAHUEL",
"NAHYAN",
"NAHYLA",
"NAIARA",
"NAICHA",
"NAILAH",
"NAILAN",
"NAILEY",
"NAILLA",
"NAILLY",
"NAILYA",
"NAILYS",
"NAIMAH",
"NAISHA",
"NAISSA",
"NAJATE",
"NAJEMA",
"NAJETE",
"NAJETT",
"NAJIBA",
"NAJIBE",
"NAJIHA",
"NAJIMA",
"NAJIME",
"NAJIYA",
"NAJLAA",
"NAJLAE",
"NAJOIE",
"NAJOUA",
"NAKADI",
"NAKADY",
"NAKANA",
"NAKANI",
"NAKIYA",
"NALEYA",
"NALHYA",
"NALINE",
"NALINI",
"NALIYA",
"NALLYA",
"NALYAH",
"NAMAKE",
"NAMIRA",
"NAMORI",
"NAMORY",
"NANABA",
"NANCIA",
"NANCIE",
"NANINE",
"NANNCY",
"NANOUK",
"NAOFEL",
"NAOIAL",
"NAOILE",
"NAOLIE",
"NAOMIE",
"NAOMYE",
"NAOUAL",
"NAOUEL",
"NAOUMI",
"NAOURA",
"NAOURI",
"NARCIS",
"NARGES",
"NARGIS",
"NARIMA",
"NARJAS",
"NARJES",
"NARJIS",
"NASERA",
"NASIHA",
"NASIMA",
"NASIME",
"NASIRA",
"NASRIA",
"NASRIN",
"NASSAR",
"NASSEM",
"NASSER",
"NASSIA",
"NASSIB",
"NASSIF",
"NASSIM",
"NASSIN",
"NASSIR",
"NASSMA",
"NASSOU",
"NASSRA",
"NASSRI",
"NASSUF",
"NASSUR",
"NASSYA",
"NASSYM",
"NASTIA",
"NASTYA",
"NATAEL",
"NATALE",
"NATALI",
"NATALY",
"NATANE",
"NATANN",
"NATASA",
"NATHAN",
"NATHAO",
"NATHEA",
"NATHEO",
"NATHIS",
"NATHYS",
"NATIVA",
"NATTAN",
"NATTEO",
"NATTIE",
"NAULAN",
"NAURIA",
"NAVEED",
"NAVEEN",
"NAWAEL",
"NAWALE",
"NAWELE",
"NAWELL",
"NAWENN",
"NAWFAL",
"NAWFEL",
"NAWRAS",
"NAWRES",
"NAYANA",
"NAYANE",
"NAYANN",
"NAYARA",
"NAYATI",
"NAYCHA",
"NAYDEN",
"NAYELI",
"NAYILA",
"NAYIMA",
"NAYIRA",
"NAYLAH",
"NAYLAN",
"NAYLIA",
"NAYLIE",
"NAYLIS",
"NAYLLA",
"NAYSAM",
"NAYSHA",
"NAYSON",
"NAYSSA",
"NAYTON",
"NAZARE",
"NAZEHA",
"NAZIFE",
"NAZIHA",
"NAZILA",
"NAZIMA",
"NAZIME",
"NAZIRA",
"NAZIRE",
"NDELLA",
"NDIAGA",
"NDIAYE",
"NEBILA",
"NECATI",
"NEDJIM",
"NEDJMA",
"NEELAH",
"NEELAM",
"NEELAN",
"NEFELI",
"NEFISE",
"NEGUIB",
"NEHAMA",
"NEHEMI",
"NEHEMY",
"NEHILA",
"NEHLAN",
"NEHLIA",
"NEHUEN",
"NEHYLA",
"NEIJMA",
"NEILAH",
"NEILAN",
"NEILLA",
"NEILYA",
"NEISHA",
"NEISSA",
"NEJOUA",
"NELCIA",
"NELCIE",
"NELCYA",
"NELHYA",
"NELIAH",
"NELIDA",
"NELINA",
"NELITA",
"NELIYA",
"NELLIA",
"NELLIE",
"NELLYA",
"NELSIA",
"NELSIE",
"NELSON",
"NELSSY",
"NELSYA",
"NELVIN",
"NELYAH",
"NEMATI",
"NEOMIE",
"NERGIS",
"NERGIZ",
"NERINA",
"NERMIN",
"NESIBE",
"NESIDA",
"NESLIE",
"NESLIN",
"NESRIN",
"NESSAH",
"NESSIA",
"NESSIE",
"NESSIM",
"NESSMA",
"NESSYA",
"NESSYM",
"NESTOR",
"NETTIE",
"NEVAEH",
"NEVENA",
"NEVINE",
"NEVZAT",
"NEWFEL",
"NEWMAN",
"NEWTON",
"NEYLAH",
"NEYLAN",
"NEYLIA",
"NEYLIO",
"NEYLLA",
"NEYMAR",
"NEYSHA",
"NEYSSA",
"NEYTON",
"NEZIHA",
"NGUYEN",
"NHOLAN",
"NIAMEY",
"NIATOU",
"NIBRAS",
"NICKIE",
"NICOLA",
"NICOLE",
"NICOLO",
"NIDALE",
"NIDHAL",
"NIEVES",
"NIHADE",
"NIHALE",
"NIHELE",
"NIKHIL",
"NIKITA",
"NIKLAS",
"NIKOLA",
"NILANI",
"NILGUN",
"NILSEN",
"NILSON",
"NILTON",
"NIMROD",
"NINNOG",
"NIOUMA",
"NIRINA",
"NISHAN",
"NISRIN",
"NISSAF",
"NISSAN",
"NISSIA",
"NISSIM",
"NISSYA",
"NITHYA",
"NITISH",
"NIVEDA",
"NIVINE",
"NIYAZI",
"NIZARD",
"NIZARE",
"NOALIE",
"NOALIG",
"NOAMAN",
"NOANNE",
"NOBERT",
"NOELAN",
"NOELIA",
"NOELIE",
"NOELIG",
"NOELLA",
"NOELLE",
"NOELLY",
"NOELYA",
"NOELYN",
"NOELYS",
"NOEMIA",
"NOEMIE",
"NOEMYE",
"NOEVAN",
"NOGAYE",
"NOHAME",
"NOHANE",
"NOHANN",
"NOHELA",
"NOHEMY",
"NOHLAN",
"NOJOUD",
"NOLAAN",
"NOLAHN",
"NOLANE",
"NOLANN",
"NOLEEN",
"NOLENE",
"NOLHAN",
"NOLINE",
"NOLITA",
"NOLLAN",
"NOLVEN",
"NOLWAN",
"NOLWEN",
"NOLYAN",
"NOLYNE",
"NOMANE",
"NOMENA",
"NORANE",
"NORAYA",
"NORDEN",
"NORDIN",
"NOREEN",
"NORENE",
"NORHAN",
"NORHEN",
"NORIAN",
"NORICK",
"NORINA",
"NORINE",
"NORLAN",
"NORMAN",
"NORRIS",
"NORTON",
"NORYNE",
"NOUAIM",
"NOUARA",
"NOUARI",
"NOUAYM",
"NOUFEL",
"NOUHAD",
"NOUHOU",
"NOUMAN",
"NOUMEA",
"NOUMOU",
"NOURAH",
"NOURAN",
"NOURIA",
"NOUROU",
"NOURRA",
"NOURYA",
"NOUSRA",
"NOUZHA",
"NOUZLA",
"NOVANN",
"NOVICA",
"NOWFEL",
"NOWLAN",
"NOYALE",
"NSIMBA",
"NUCCIA",
"NUNCIA",
"NUNZIA",
"NUNZIO",
"NURCAN",
"NURDAN",
"NURGUL",
"NURHAN",
"NURIYE",
"NURSEL",
"NURSEN",
"NURTEN",
"NUSRET",
"NYLANN",
"NYLSON",
"NYOUMA",
"NYSSIA",
];
}